import React from 'react';
import { browserHistory, Link } from 'react-router';

/**
 * A Link-like component that will execute a specified function and/or navigate
 * to a route when clicked.
 *
 * @param {Object} props
 * @param {React.Component | React.Component[]} props.children - Content to
 * display inside the menu frame.
 * @param {string} props.to - The route to navigate to when component is
 * clicked.
 * @param {[function]} props.onClick - A function to execute when component is
 * clicked.
 * @param {[string]} classes - CSS classes to apply to the containing div.
 * @returns {React.Component} The Link and contained children.
 */
export default function DoLink({ children, to, onClick, classes }) {

  function executeAndGo() {
    if (onClick instanceof Function) {
      onClick();
    }
    if (to) {
      browserHistory.push(to);
    }
  }

  return (
    <Link
       onClick={ executeAndGo }
       className={ classes }
    >
      { children }
    </Link>
  );
}
