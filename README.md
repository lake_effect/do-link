# do-link

A Link-like component that will execute a specified function and/or navigate to
a route when clicked.

The default react-router Link doesn't do both navigation and click listening
(i.e., the navigation prevents the default action), so this gives you that
choice back.

## how it works

It's simple:

```js
export function DoLink({ children, to, onClick, classes }) {

  function executeAndGo() {
    if (onClick instanceof Function) {
      onClick();
    }
    if (to) {
      browserHistory.push(to);
    }
  }

  return (
    <Link
       onClick={ executeAndGo }
       className={ classes }
    >
      { children }
    </Link>
  );
}
```
